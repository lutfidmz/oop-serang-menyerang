<?php
trait hewan
{
    public $nama;
    public $darah = 50;
    public $jumlah_kaki;
    public $keahlian;

    public function set_nama($nama)
    {
        $this->nama = $nama;
        return $this;
    }
    public function set_darah($darah)
    {
        $this->darah = $darah;
        return $this;
    }
    public function set_jumlah_kaki($jumlah_kaki)
    {
        $this->jumlah_kaki = $jumlah_kaki;
        return $this;
    }
    public function set_keahlian($keahlian)
    {
        $this->keahlian = $keahlian;
        return $this;
    }

    public function atraksi()
    {
        return $this->nama . ' sedang ' . $this->keahlian;
    }

    public function get_nama()
    {
        return $this->nama;
    }
}

// $burung = new hewan();
// $burung->set_nama('kutilang')->set_darah(75)->set_jumlah_kaki(2)->set_keahlian('terbang tinggi');

// echo $burung->atraksi();
