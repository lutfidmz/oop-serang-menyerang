<?php
require_once 'fight.class.php';
require_once 'hewan.class.php';

class Elang
{
    use hewan, fight;
    public function getInfoHewan()
    {
        echo 'Nama : ' . $this->nama . '<br>';
        echo 'Darah : ' . $this->darah . '<br>';
        echo 'Jumlah_kaki : ' . $this->jumlah_kaki . '<br>';
        echo 'Keahlian : ' . $this->keahlian . '<br>';
        echo 'Attack Power : ' . $this->attackPower . '<br>';
        echo 'Defense Power : ' . $this->defensePower . '<br><br>';
    }
}

class Harimau
{
    use hewan, fight;
    public function getInfoHewan()
    {
        echo 'Nama : ' . $this->nama . '<br>';
        echo 'Darah : ' . $this->darah . '<br>';
        echo 'Jumlah_kaki : ' . $this->jumlah_kaki . '<br>';
        echo 'Keahlian : ' . $this->keahlian . '<br>';
        echo 'Attack Power : ' . $this->attackPower . '<br>';
        echo 'Defense Power : ' . $this->defensePower . '<br>';
    }
}

$elang = new Elang();
$elang->set_nama('Kutilang')->set_jumlah_kaki(2)->set_keahlian('terbang tinggi')->set_attack_power(10)->set_defense_power(5);
echo $elang->atraksi() . '<br>';
echo $elang->getInfoHewan();

$harimau = new Harimau();
$harimau->set_nama('Harimau Raja')->set_jumlah_kaki(4)->set_keahlian('lari cepat')->set_attack_power(7)->set_defense_power(8);
echo $harimau->atraksi() . '<br>';
echo $harimau->getInfoHewan() . '<br><br>';

echo $elang->serang($harimau);
echo '<br>';
echo '<br>';


echo $harimau->getInfoHewan() . '<br><br>';
echo $elang->getInfoHewan();
