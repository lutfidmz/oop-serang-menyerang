<?php
trait fight
{

    public $attackPower;
    public $defencePower;

    public function set_attack_power($attackPower)
    {
        $this->attackPower = $attackPower;
        return $this;
    }

    public function set_defense_power($defensePower)
    {
        $this->defensePower = $defensePower;
        return $this;
    }


    public function serang($serang)
    {
        echo $this->nama . ' sedang menyerang ' . $serang->nama . '<br>';
        $this->diserang($serang);
    }

    public function diserang($diserang)
    {
        $diserang->darah = $diserang->darah - $this->attackPower / $diserang->defensePower;
        echo $diserang->nama . ' sedang diserang ' . $this->nama;
    }
}
